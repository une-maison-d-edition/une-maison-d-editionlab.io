#!/bin/sh

convert $1 -resize 640x640 -sharpen 0x2.0 -dither FloydSteinberg -colors 8 ${1%.*}.png 
